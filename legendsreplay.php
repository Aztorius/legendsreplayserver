<?php
$api_key='';

header('Content-type: application/json; charset=utf-8');

if(isset($_GET['platformid'])&&isset($_GET['summonerid'])){
    $summonerid = $_GET['summonerid'];
    $platformid = $_GET['platformid'];

    $json = file_get_contents('https://'.strtolower($platformid).'.api.riotgames.com/lol/spectator/v3/active-games/by-summoner/'.$summonerid.'?api_key='.$api_key);

    if(isset($json)){
        echo $json;
    }
}
elseif(isset($_GET['region'])&&isset($_GET['summonername'])){
    $summonername = $_GET['summonername'];
    $region = $_GET['region'];

    try
    {
        $bdd = new PDO('mysql:host=localhost;dbname=legendsreplay;charset=utf8', 'root', '');
    }
    catch(Exception $e)
    {
        die('Erreur : '.$e->getMessage());
    }

    $req = $bdd->prepare('SELECT * FROM summoners WHERE region=? AND normname=?');
    $req->execute(array($region,str_replace( ' ', '',strtolower($summonername))));
    $data = $req->fetch();
    $req->closeCursor();

    if(empty($data)){

        $regionserver = array(
            'EUW' => 'euw1',
            'EUNE' => 'eun1',
            'NA' => 'na1',
            'JP' => 'jp1',
            'KR' => 'kr',
            'OCE' => 'oc1',
            'BR' => 'br1',
            'LAN' => 'la1',
            'LAS' => 'la2',
            'RU' => 'ru',
            'TR' => 'tr1',
            'PBE' => 'pbe'
        );

        $json = file_get_contents('https://'.$regionserver[$region].'.api.riotgames.com/lol/summoner/v3/summoners/by-name/'.$summonername.'?api_key='.$api_key);

        if(isset($json)){
            $data = json_decode($json);
            $sumname = strtolower($data->{'name'});

            $req = $bdd->prepare('INSERT INTO summoners(region, id, name, normname, profileIconId, revisionDate, summonerLevel) VALUES(:region, :id, :name, :normname, :profileIconId, :revisionDate, :summonerLevel)');
            $req->execute(array(
                'region' => $region,
                'id' => $data->{'id'},
                'name' => $data->{'name'},
                'normname' => $sumname,
                'profileIconId' => $data->{'profileIconId'},
                'revisionDate' => $data->{'revisionDate'},
                'summonerLevel' => $data->{'summonerLevel'}
            ));
        }
    }
    else{
        $json = '{"' . $data['normname'] . '":' . json_encode(array('id' => $data['id'], 'name' => $data['name'], 'profileIconId' => $data['profileIconId'], 'revisionDate' => $data['revisionDate'], 'summonerLevel' => $data['summonerLevel'] )) . '}';
    }

    if(isset($json)){
        echo $json;
    }
}
?>
